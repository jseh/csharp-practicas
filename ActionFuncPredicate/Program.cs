﻿using System;

namespace ActionFuncPredicate
{
    class Program
    {
        static void Main(string[] args)
        {

            #region

                //Action
                //delegado para funcion que puede tomar parametros y NO regresa un valor

                //Func
                //delegado para funcion que puede tomar parametros y regresa un valor

                //Predicate
                //Version especializada de Func, toma un argumento, evalua un valor contra un conjunto de criterios y siempre regresa un valor booleano

            #endregion


            Console.WriteLine("Prueba");

            RecibirFunciones(2, DoblarNumero, ImprimirMensaje, EstaEnMayuscula);
            Console.ReadLine();


        }
       
        static void RecibirFunciones(
            int n,
            // p1, p2, retorno
            Func<int, int> fn,
            // p1, p2,
            Action<string> ac,
            // p1, p2, retorno bool
            Predicate<string> pr
            )
        {
            Console.WriteLine(fn(2));
            ac("hola");

            if (!pr("hola"))
            {
                Console.WriteLine("Esta en minuscula");
            }
        }

        // Funcion
        static int DoblarNumero(int n)
        {
            return n*2;
        }

        // Accion
        static void ImprimirMensaje(string m)
        {
            Console.WriteLine(m);
        }

        // Predicado
        static bool EstaEnMayuscula(string str)
        {
            return str.Equals(str.ToUpper());
        }
    }
}
