﻿using System;

using System.Threading;
using System.Threading.Tasks;

namespace dotnet_core_pruebas
{
    class Program
    {
        // Una cosa que debemos tener en cuenta es que una vez que se cancela una tarea,
        // la propiedad IsCancellationRequested nos devolverá siempre true, ya que es una 
        // propiedad de sólo lectura, por lo que para reutilizar CancellationToken, 
        // deberíamos volver a crear un objeto de tipo CancellationTokenSource para luego
        //  asignarle a CancellationToken un nuevo token.
        // Es decir, cada tarea que deseemos procesar y en su caso cancelar, debe llevar
        // su propio token.

        private static int counter = 0;
        
        static void Main(string[] args)
        {
            Console.WriteLine("Started");
            Console.WriteLine();

            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            CancellationToken cancellationToken = cancellationTokenSource.Token;

            // ejecuta la funcion inmediatamente
            Task task = Task.Run(() =>
            {
                while (!cancellationToken.IsCancellationRequested)
                {
                    counter++;

                    Console.Write($"{counter}|");
                    Thread.Sleep(500);
                }
            }, cancellationToken);

            // Task task = Task.Run(() => Process(cancellationToken));


            Console.WriteLine("Press enter to stop the task");
            Console.ReadLine();
            cancellationTokenSource.Cancel();
            Console.WriteLine($"Task executed {counter} times");

            Console.WriteLine();
            Console.WriteLine("Press any key to close");
            Console.ReadKey();
            


        }


        private static void Process(CancellationToken cancellationToken)
        {
            while (true)
            {
                if (cancellationToken.IsCancellationRequested)
                {
                    return;
                }

                counter++;

                Console.Write($"{counter}|");
                Thread.Sleep(500);
            }
        }



    }
}
