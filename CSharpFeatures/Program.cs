﻿using System.Runtime.CompilerServices;
using System;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace CSharpFeatures
{
    enum Opcion
    {
        A,
        B
    }

    class Program
    {
        static void fn<A, B>(A a, B b, A c)
        {
            Console.WriteLine(a);
            Console.WriteLine(b);
            Console.WriteLine(c);
        }
        
        
        // en la de static void cambio a async task 
        static async Task Main(string[] args)
        {
            // para metodos  genericos no siempre es necesarion indicar el tipo, lo infiere automaticamente
            fn<int, int>(20, 40, 80);
            fn(100, true, 400);
          
            
            
            Console.WriteLine("Hola");
            MetodoConParamsOpcionales(n2: 10, n1: 20);
            MetodoConParamsOpcionales(n1: 20);
            var p = new Persona()
            {
                Nombre = "Jose",
                Edad = 27
            };

            Console.WriteLine(p.Nombre);
            
            MetodoConParamsOpcionales(n1: 20, n3: p);
            
            Console.WriteLine("In C# 7.1, To use async method");
            Console.WriteLine($"Main Method execution started at {System.DateTime.Now}");
            await Task.Delay(1000);
            Console.WriteLine($"Main Method execution ended at {System.DateTime.Now}");


            (int a, int b, int c) tupla = (10, 29, 70);
            Console.WriteLine($"tupla con valor de a: {tupla.a}");
            
            StringBuilder sb = new StringBuilder("Hola");
            StringBuilder sb2 = new StringBuilder("Hola Mundoo",50);
            
            sb2.Insert(4," C#");
            Console.WriteLine(sb2);
            for(int i=0; i< sb2.Length; i++)
                Console.Write(sb2[i]);
            
            
            // sb.Append("Mundo");
            sb.AppendLine("Hello C#!");
            sb.AppendLine("This is new line.");
            string str = sb.ToString();

            Console.WriteLine();
            Console.WriteLine((int)Opcion.A);
            Console.WriteLine((int)Opcion.B);
            
            
            Console.WriteLine("Starting application...");

            CancellationTokenSource source = new CancellationTokenSource();
            //assuming the wrapping class is TplTest
            // instancia un objeto anonimo de PruebaAsync y ejecuta  CancellableTask con le token pasado que retorna una  Task
            var task = new PruebaAsync().CancellableTask(source.Token);

            Console.WriteLine("Heavy process invoked");

            Console.WriteLine("Press C to cancel");
            Console.WriteLine("");
            char ch = Console.ReadKey().KeyChar;
            if (ch == 'c' || ch == 'C')
            {
                source.Cancel();
                Console.WriteLine("\nTask cancellation requested.");
            }

            try
            {
                task.Wait();
            }
            catch (AggregateException ae)
            {
                if (ae.InnerExceptions.Any(e => e is TaskCanceledException))
                    Console.WriteLine("Task cancelled exception detected");
                else
                    throw;
            }
            finally
            {
                source.Dispose();
            }

            Console.WriteLine("Process completed");
            Console.ReadKey();



        }


        static void MetodoConParamsOpcionales(int n1, [Optional] int n2, [Optional] Persona n3)
        {
            Console.WriteLine($"n1: {n1}, n2: {n2}, n3: {n3?.Nombre}");
        }


        static (string opcionA, int opcionB) RetornarTupla()
        {
            return (opcionA: "hey you", opcionB: 23);
        }

        static void RecibirFunciones(
                // el ultimo tipo es el valor de retorno
                Func<int,int, int> sumar,
                // todos son valores de entrada
                Action<int> eliminar,
                Predicate<string> esMayuscula
            )
        {
            
        }

        
     

        
        
        
        
        private static Task<decimal> LongRunningCancellableOperation(int loop, CancellationToken cancellationToken)
        {
            Task<decimal> task = null;

            // Start a task and return it
            task = Task.Run(() =>
            {
                decimal result = 0;

                // Loop for a defined number of iterations
                for (int i = 0; i < loop; i++)
                {
                    // Check if a cancellation is requested, if yes,
                    // throw a TaskCanceledException.

                    if (cancellationToken.IsCancellationRequested)
                        throw new TaskCanceledException(task);

                    // Do something that takes times like a Thread.Sleep in .NET Core 2.
                    Thread.Sleep(1000);
                    result += i;
                }

                return result;
            });

            return task;
        }

        public static async Task ExecuteManuallyCancellableTaskAsync()
        {
            Console.WriteLine(nameof(ExecuteManuallyCancellableTaskAsync));

            using (var cancellationTokenSource = new CancellationTokenSource())
            {
                // Creating a task to listen to keyboard key press
                var keyBoardTask = Task.Run(() =>
                {
                    Console.WriteLine("Presiona enter para cancelar");
                    Console.ReadKey();

                    // Cancel the task
                    cancellationTokenSource.Cancel();
                });

                // try ejecuta tarea en paralelo, sin interumpirl flujo
                try
                {
                    var longRunningTask = LongRunningCancellableOperation(10, cancellationTokenSource.Token);
                    // espero 
                    var result = await longRunningTask;
                    Console.WriteLine("Result {0}", result);
                    Console.WriteLine("Press enter to continue");
                }
                catch (TaskCanceledException)
                {
                    Console.WriteLine("Task was cancelled");
                }
              
                // espera a que termine
                await keyBoardTask;
            }
        }


    }
}
