﻿using System;
#nullable enable
namespace Miespacio
{

    interface IGato: IAnimal{
        void maullar(){
            System.Console.WriteLine("miauuu");
        }       

        
    }

    interface IAnimal{
        // interfaces pueden tener implementacion, pero
        // no pueden ser implmentadas por otra interfaz
        void comer(){
            System.Console.WriteLine("animal comiendo por defecto");

        }
        

    }

    class Gato: IGato{
        public Gato(string color)
        {
            this.color = color;
        }
        
        public string? Nombre { get; set; }

        string? color;


        // public void comer(){
        //     System.Console.WriteLine("gato comiendo..");

        // }
    }

    class Oso 
    {
        protected int color = 2;
    }

    class OsoPolar: Oso
    {
        public void mostrarColor(){
            // Console.WriteLine(base.color);
            Console.WriteLine(color);
        }

    }




    class Program
    {
        static void Main(string[] args)
        {
            // IAnimal g = new Gato("Blanco");
            // g.comer();
            // ((IGato)g).maullar();

            Gato g2 = new Gato("Cafe");
            // si se necesita acceder a la implementacion por defecto
            // castear a la interfaz que contiene el metodo por defecto
            // solo se podra acceder al metodo por defecto si no se ha implmentado 
            ((IAnimal)g2).comer();



            var op = new OsoPolar();
            op.mostrarColor();

        }
    }
}
