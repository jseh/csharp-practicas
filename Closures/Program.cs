﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Closures
{
    class Program
    {
        static void Main(string[] args)
        {
            //  regresa la referencia a la funcion, junto con una copia del estado que tenía ObtenerFuncion()
            var miFn = ObtenerFuncion();
            Console.WriteLine(miFn(1));
            Console.WriteLine(miFn(2));

            Console.WriteLine();


            var miFn2 = ObtenerFuncion();
            Console.WriteLine(miFn2(4));
            Console.WriteLine(miFn2(5));
            
            Console.WriteLine();
            




            string x = "mensaje original";
            Action miaccion = () => {
                Console.WriteLine(x);
                x = "ultimo cambio";
                Console.WriteLine(x);
            };

            x = "primer cambio";
            // la funcion tiene el estado generado antes de la ejecucion  de la misma
            miaccion();

            x = "cambio desde la ultima llamada";
            miaccion();



            Console.ReadLine();

        }

        public static Func<int, int> ObtenerFuncion()
        {
            var valorinicial = 2;

            var estado = 0;
            // retorna una funcion anonima o lambda, que crea un closure es decir guarda el estado del scope exterior
            Func<int, int> fn = delegate (int var1)
            {
                estado += 1;
                Console.WriteLine($"el estado que mantiene la funcion es: {estado}");

                valorinicial = valorinicial + var1;
                return valorinicial;
            };

            return fn;
        }
    }
}
