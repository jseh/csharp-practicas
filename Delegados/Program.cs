﻿using System;

namespace Delegados
{
    class Program
    {
        public delegate int OpCalulo(int x);
        public delegate int OpCalulo2(int x, int y);

        // solo delegados genericos pueden sobrecargarse
        public delegate R SampleGenericDelegate<A, R>(A a);
        public delegate void SampleGenericDelegate<A>(A a);

        static void Main(string[] args)
        {
            OpCalulo del = x => x * x;
            int j = del(2);

            OpCalulo2 del2 = Sumar;
            int k = del2(2, 2);

            OpCalulo2 del3 = new OpCalulo2(Restar);
            del3 += Sumar;
            // los delegados retornan el valor de la ultima funcion anexada
            int z = del3(2,2);
            Console.WriteLine(z);

            // params, valor retorno
            SampleGenericDelegate<int, string> del4 = ImprimirCarita;
            SampleGenericDelegate<int> del5 = MostrarMensaje;


            // covarianza y la contravarianza

            Console.ReadKey();

        }

        static string ImprimirCarita(int x)
        {
            return ":)";
        }

        static void MostrarMensaje(int x)
        {
            Console.WriteLine("Hola");
        }


        static int Sumar(int x, int y)
        {
            Console.WriteLine("Sumando");
            return x + y;
        }

        static int Restar(int x, int y)
        {
            Console.WriteLine("Restar");
            return x - y;
        }
    }
}
