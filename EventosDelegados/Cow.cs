﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventosDelegados
{
    class Cow
    {
        public string Name { get; internal set; }
        //EventHandler es un delegado
        public event EventHandler Moo;



        private Action evento;

        public event Action Evento
        {
            add
            {
                evento += value;
            }
            remove
            {
                evento -= value;
            }

        }

        public delegate void ChangedEventHandler(object sender, EventArgs e);
        //public delegate string ChangedEventHandler(object sender, EventArgs e);
        public event ChangedEventHandler Changed;

        //Simula eventos que han ocurrido 
        public void BeTippedOver()
        {
            Moo?.Invoke(this, EventArgs.Empty);
            Moo?.Invoke(this, EventArgs.Empty);
        }
    }
}
