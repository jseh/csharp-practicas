﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventosDelegados
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("El ejemplo del videoEncoder");
            var video = new Video() { Tittle = "Video 1" };
            var videoEncoder = new VideoEncoder();

            var mailService = new MailService();
            var messegeService = new MessegeService();

            videoEncoder.VideoEncoded += mailService.OnVideoEncoded;
            videoEncoder.VideoEncoded += messegeService.OnVideoEncoded;
            videoEncoder.Encode(video);



            Console.WriteLine("### El ejemplo de la vaca");
            Cow c1 = new Cow { Name = "Betsy" };
            c1.Moo += giggle;
            Cow c2 = new Cow { Name = "Georgy" };
            c2.Moo += giggle;
            Cow v = new Random().Next() % 2 == 0 ? c1 : c2;
            v.BeTippedOver();


           


        }

        static void giggle(object sender, EventArgs e)
        {
            Cow c = sender as Cow;
            Console.WriteLine("Soy la vaca "+ c.Name +" moo!");
        }



    }

  
}
