﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MapReduceFilter
{
    class Program
    {
        static void Main(string[] args)
        {
            //Map o Select
            var lista1 = new List<int> { 2, 3, 4, 5};
            var seleccionados = lista1.Select(x => x * x).ToList<int>();
            seleccionados.ForEach(x => Console.WriteLine(x));



            //Filter o Where
            var lista2 = new List<string> {
                "a.jpg",
                "b.jpg",
                "c.png",
                "d.png",
                "f.jpg",
                "g.png"
            };
            var filtrados = lista2
                            .Where(x => x.EndsWith(".png"))
                            .ToList(); 
            filtrados.ForEach(x => Console.WriteLine(x));


            var manzanas = new List<Manzana>
            {
                new Manzana()
                {
                    Tipo = "M1",
                    Color = "Rojo",
                    Tamaño = 5
                },
                new Manzana()
                {
                    Tipo = "M2",
                    Color = "Verde",
                    Tamaño = 10
                }
            };

            var manzanasFiltradas = manzanas.Where(x => x.Tamaño > 1).ToList();
            manzanasFiltradas.ForEach(x => Console.WriteLine(x.Tamaño));




            //Reduce o Aggregate
            var lista3 = new List<int> { 2, 3, 4, 5};
            var suma = lista3.Aggregate((acc, x) => acc + x + 1);
            Console.WriteLine(suma);


            Console.ReadKey();
        }
    }
}
