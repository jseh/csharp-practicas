﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Yield
{
    class Persona
    {
        public string Nombre { get; set; }
        public string Color { get; set; }
    }

    class Program
    {
        // yield convierte una funcion en un generador
        // Usar yield , elimina la necesidad de definir una clase explícita extra para implementar un iterador.

        static public IEnumerable<int> GeneradorEnteros()
        {
            yield return 1;
            yield return 2;
            yield return 4;
            yield return 8;
            yield return 16;
            yield return 16777216;
        }

        

        static void Main(string[] args)
        {

            var e = GeneradorEnteros().GetEnumerator();

            e.MoveNext();
            Console.WriteLine(e.Current);

            e.MoveNext();
            Console.WriteLine(e.Current);

            // foreach llama en cada iteracion al metodo moveNext()
            //foreach (int i in GeneradorEnteros())
            //{
            //    Console.WriteLine(i.ToString());
            //}



            Console.WriteLine();
            // ###  List Comprehensions
            // [x*x for x in range(10)]
            //Enumerable.Range(0, 10).Select(x => x * x).ToList();
            Enumerable.Range(0, 10).Select(x => x * x).ToList().ForEach(x => Console.WriteLine(x));
            
            // [x*x for x in range(10) if x%2]
            // Enumerable.Range(0, 10).Where(x => x % 2 != 0).Select(x => x * x).ToList();
            Enumerable.Range(0, 10).Where(x => x % 2 != 0).Select(x => x * x).ToList().ForEach(x => Console.WriteLine(x));


            // Dictionary comprehensions

            //var fruit = new[] { "apples", "oranges", "bananas", "pears" };

            //fruit.Select(f => f.Length).ToList();

            //new HashSet<int>(fruit.Select(f => f.Length));

            //fruit.ToDictionary(f => f, f => f.Length);


            // Set comprehensions



            Console.ReadKey();
        }
    }
}
